import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';

import { IEventCard } from '../../../shared/models/event.model';

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
})
export class EventComponent {

  @Input() event: IEventCard = {
    id: 0,
    title: '',
    startAt: '',
    endAt: '',
    description: '',
    tz: '',
    x: 0,
    y: 0,
    h: 0,
    w: '112px',
    guests: [],
    isGuest: false,
  };

  constructor(private router:Router) { }

  editEvent(){
		this.router.navigate([`calendar/${this.event.id}`]);
	}
}
