import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';

import { LoginService } from '../login.service';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
})
export class SigninComponent {
  signinForm = this.formBuilder.group({
    email: '',
    password: '',
  });
  loading: boolean = false;
  error: any;

  constructor(
    private formBuilder: FormBuilder,
    private loginService: LoginService,
  ) { }

  onSubmit(): void {
    let value = this.signinForm.value;
    this.loginService.signin(value);
  }
}
