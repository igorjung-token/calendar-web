import { Injectable } from '@angular/core';
import {Apollo, gql} from 'apollo-angular';
import { Router } from '@angular/router';

import {
  IUser,
  ILogin,
  ResponseLogin,
  ResponseCreateUser
 } from '../shared/models/user.model';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  user: IUser = {
    id: 0,
    email: '',
    token: '',
  }

  constructor(private router: Router, private apollo: Apollo) { }

  signin(user: ILogin) {
    this.apollo.mutate<ResponseLogin>({
      mutation: gql`
        mutation Login($data: UserInput!) {
          login(data: $data) {
            id
            email
            token
          }
        }
      `,
      variables: {
        data: user,
      },
    }).subscribe(({ data }) => {
      localStorage.setItem('user', JSON.stringify(data?.login));
      this.router.navigate(['/calendar']);
    });
  }

  signup(user: ILogin) {
    this.apollo.mutate<ResponseCreateUser>({
      mutation: gql`
        mutation createUser($data: UserInput!) {
          createUser(data: $data) {
            id
            email
            token
          }
        }
      `,
      variables: {
        data: user,
      },
    }).subscribe(({ data }) => {
      localStorage.setItem('user', JSON.stringify(data?.createUser));
      this.router.navigate(['/calendar']);
    });
  }
}
