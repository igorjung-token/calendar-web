import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'monthOfYear'
})
export class MonthOfYearPipe implements PipeTransform {
  transform(value: string): string {
    return moment(value).format('MMMM YYYY');
  }
}
