import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'hour'
})
export class HourPipe implements PipeTransform {
  transform(value: string): string {
    return moment()
      .set({
        hour: parseInt(value.substring(0, 2), 10),
        minute: 0
      })
      .format('h A');
  }
}
