import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'hourOfDay'
})
export class HourOfDayPipe implements PipeTransform {
  transform(value: string): string {
    return moment(value).format('h A');
  }
}
